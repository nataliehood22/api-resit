var request = require('request')

const appID = "AIzaSyD-w3FVP5yfGqE_0WVbVub7XuW6rqLICcg"

exports.search = function(query, callback) {
    console.log('search')
    const url = "https://www.googleapis.com/books/v1/volumes"
    const query_string = {q: query}
    request.get({url: url, qs: query_string}, function(err, res, body) {
        if (err) {
            callback({code:500, response:{status: 'error', message: 'search has failed', data: err}})
        }
        console.log(typeof body)
        const json = JSON.parse(body)
        const items = json.items
        const books = items.map(function(element) {
            return {id:element.id, title:element.volumeInfo.title, authors:element.volumeInfo.authors}
        })
        console.log(books)
        callback({code:200, response:{status:'successful', message:books.length+' books have been found', data:books}})
    })
}